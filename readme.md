# Dotnet Sonarscanner

[![dotnet-sonarscanner](https://img.shields.io/badge/dotnet--sonarscanner-0.9-brightgreen.svg)](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/)
 [![sonarscanner](https://img.shields.io/badge/sonarscanner-5.0.4-brightgreen.svg)](https://www.nuget.org/packages/dotnet-sonarscanner)
 [![outdated](https://img.shields.io/badge/outdated-3.1.1-brightgreen.svg)](https://www.nuget.org/packages/dotnet-outdated-tool/)
 [![java](https://img.shields.io/badge/java-11.0.9.1-brightgreen.svg)](https://www.java.com/)
 [![dotnet-runtime](https://img.shields.io/badge/dotnet--runtime-5.0.2-brightgreen.svg)](https://dotnet.microsoft.com/)
 [![dotnet-sdk](https://img.shields.io/badge/dotnet--sdk-5.0.102-brightgreen.svg)](https://dotnet.microsoft.com/)
 [![nodejs](https://img.shields.io/badge/nodejs-14.15.4-brightgreen.svg)](https://nodejs.org/en/)

[![Docker Stars](https://img.shields.io/docker/stars/devgem/dotnet-sonarscanner-base.svg)](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base 'DockerHub')
 [![Docker Pulls](https://img.shields.io/docker/pulls/devgem/dotnet-sonarscanner-base.svg)](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base 'DockerHub')
 [![Docker Layers](https://images.microbadger.com/badges/image/devgem/dotnet-sonarscanner-base.svg)](https://microbadger.com/images/devgem/dotnet-sonarscanner-base)
 [![Docker Version](https://images.microbadger.com/badges/version/devgem/dotnet-sonarscanner-base.svg)](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base)

This docker image can be used as a base image for analyzing netcore projects in a CI pipeline.

To build the image:

```shell
export DOTNET_SONARSCANNER_VERSION="0.9"
docker build --pull --no-cache -t devgem/dotnet-sonarscanner-base:$DOTNET_SONARSCANNER_VERSION --build-arg VERSION="$DOTNET_SONARSCANNER_VERSION" --build-arg VCS_REF=`git rev-parse --short HEAD` --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` .
```

## Usage

```Dockerfile
# use the image as a base image to analyze your projects
FROM devgem/dotnet-sonarscanner-base:0.9

ARG PIPELINEID
ARG SONARQUBE_TOKEN

WORKDIR /src
COPY . .

RUN dotnet sonarscanner begin /key:"your-key" /name:"your-name" /version:"${PIPELINEID}" /o:your-organization /d:sonar.login=${SONARQUBE_TOKEN}  /d:sonar.host.url=https://sonarcloud.io /d:sonar.coverage.exclusions=**/*.*
RUN dotnet build your-solution.sln
RUN dotnet sonarscanner end /d:sonar.login=${SONARQUBE_TOKEN}
```

Replace the whole `your-*` by your stuff. Replace the `sonar.host.url` with the url of your SonarQube instance. Add your analysis arguments with `/d:`.

## Versions

Pre-built images are available on Docker Hub:

| image | date | sonarscanner | openjdk | dotnet runtime | dotnet sdk | nodejs | outdated | remarks |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| [`devgem/dotnet-sonarscanner-base:0.9`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20210202 | 5.0.4 | 11.0.9.1 | 5.0.2 | 5.0.102 | 14.15.4 | 3.1.1 |  |
| [`devgem/dotnet-sonarscanner-base:0.8`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20210104 | 5.0.4 | 11.0.9.1 | 5.0.0 | 5.0.100 | 14.15.3 | 3.1.1 |  |
| [`devgem/dotnet-sonarscanner-base:0.7`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20201217 | 5.0.4 | 11.0.9.1 | 5.0.0 | 5.0.100 | 14.15.2 | 3.1.0 |  |
| [`devgem/dotnet-sonarscanner-base:0.6`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20201120 | 5.0.4 | 11.0.9 | 5.0.0 | 5.0.100 | 14.15.1 | 3.1.0 |  |
| [`devgem/dotnet-sonarscanner-base:0.5`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20201117 | 5.0.4 | 11.0.9 | 5.0.0 | 5.0.100 | 14.15.1 | NA |  |
| [`devgem/dotnet-sonarscanner-base:0.4`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20201110 | 5.0.3 | 11.0.9 | 5.0.0 | 5.0.100 | 14.15.0 | NA | |
| [`devgem/dotnet-sonarscanner-base:0.3`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20201103 | 5.0.1 | 11.0.9 | 3.1.9 | 3.1.403 | 11.15.0 | NA | |
| [`devgem/dotnet-sonarscanner-base:0.2`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20201017 | 4.10 | 11.0.8 | 3.1.9 | 3.1.403 | NA | NA | |
| [`devgem/dotnet-sonarscanner-base:0.1`](https://hub.docker.com/r/devgem/dotnet-sonarscanner-base/tags/) | 20201017 | NA | NA | NA | NA | NA | NA | |

## Updating on MicroBadger

```shell
curl -X POST https://hooks.microbadger.com/images/devgem/dotnet-sonarscanner-base/wfaqXFsnP4-QMyafbHnFId79ZvM=
```
