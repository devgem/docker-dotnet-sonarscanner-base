FROM mcr.microsoft.com/dotnet/sdk:5.0

ARG VCS_REF
ARG VERSION
ARG BUILD_DATE

LABEL \
    org.label-schema.schema-version="1.0" \
    org.label-schema.name="dotnet-sonarscanner-base" \
    org.label-schema.description="Image can be used in a buildpipeline to analyse a dotnet project with 'dotnet sonarscanner'." \
    org.label-schema.vcs-url="https://gitlab.com/devgem/docker-dotnet-sonarscanner-base" \
    org.label-schema.vendor="DevGem" \
    org.label-schema.vcs-ref=$VCS_REF \
    version=$VERSION \
    build-date=$BUILD_DATE

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install java
RUN mkdir -p /usr/share/man/man1 && \
    apt-get update && \
    apt-get install -y --no-install-recommends openjdk-11-jdk-headless && \
    apt-get install -y --no-install-recommends ant && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
# Set java path
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/
RUN export JAVA_HOME

# Set .NET tools path
ENV PATH="$PATH:/root/.dotnet/tools"
# install sonarscanner
RUN dotnet tool install --global dotnet-sonarscanner
# install outdated
RUN dotnet tool install --global dotnet-outdated-tool

# install NodeJS
RUN apt-get update && \
    apt-get -y install --no-install-recommends curl gnupg && \
    curl -sL https://deb.nodesource.com/setup_14.x  | bash - && \
    apt-get -y install --no-install-recommends nodejs && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN \
    echo '' && \
    echo '********' && \
    echo 'VERSIONS' && \
    echo '********' && \
    echo '' && \
    echo 'java:' && \
    java -version && \
    echo '' && \
    echo 'dotnet:' && \
    dotnet --info && \
    echo '' && \
    echo 'outdated:' && \
    dotnet-outdated --version && \
    echo '' && \
    echo 'nodejs:' && \
    node -v && \
    echo '' && \
    echo '********' && \
    echo ''
